my_list = ["Evander", 22, True]
print(my_list)

days_of_week = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
]
print(days_of_week)

#adding to a list using append method
favorite_foods = []

food = input("What is one of your favorite foods? ")
favorite_foods.append(food)

food = input("What is another one of your favorite foods? ")
favorite_foods.append(food)

food = input("Last time, what is one of your favorite foods? ")
favorite_foods.append(food)

print("Your favorite foods are", favorite_foods)

#length of a list
num_foods = len(favorite_foods)
print("That's", num_foods, "of your favorite foods.")

#indexing
print("The first day of the week is", days_of_week[0])

#setting items by index
days_of_week[3] = "Hump Day!"
days_of_week[5] = "FriYAY!"
print(days_of_week)
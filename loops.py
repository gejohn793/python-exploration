months_in_year = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]
'''for loop, note month_name variable is assigned to each index in the list
for month_name in months_in_year:
    print(month_name)'''

''' for loop with if statement
print("Months with names that have")
print("more than eight letters: ")

for month_name in months_in_year:
    if len(month_name) > 8:
        print(month_name)'''

'''looping certain number of times
for num in range(5):
    print(num)'''

#using for loops to replace multiple iterations of append method
favorite_foods = []

for num in range (3):
    food = input("What is one of your favorite foods? ")
    favorite_foods.append(food)

print("Your favorite foods are", favorite_foods)

#infinite list
infinite_list = [1]
for item in infinite_list:
    print(item,len(infinite_list))
    infinite_list.append(item+1)
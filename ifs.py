string_age = input("How old are you? ")
int_age = int(string_age)


if int_age > 17:
    string_cash_on_hand = input("How much cash do you have on you? ")
    int_cash_on_hand = int(string_cash_on_hand)

    if int_cash_on_hand > 8:
        print("You can buy a lotto ticket!\nLet's see how many you can get!")
    else:
        print("You are old enough to buy a lottery ticket, but you do not have enough cash on hand to buy one.")
else:
    print("You may not buy a lottery ticket.\nCan I interest you in some candy?")

print("Thank you for your patronage.") #this last print line is not indented and will always run


if int_age > 12 and int_age < 20:
    is_teenager = True
else:
    is_teenager = False

if is_teenager:
    print("Are you on Tiktok?")
else:
    print("Are you on Facebook?")

"""ELIF
age = 23
cash_on_hand = 5

if age > 17 and cash_on_hand > 8:
    print("You can buy a lottery ticket.")
    print("How many would you like?")
elif age > 17 and cash_on_hand <= 8:
    print("You don't have enough money.")
    print("Please, come back when you get more.")
else:
    print("You may not buy a lottery ticket.")
    print("Can I interest you in some candy?")

print("Thank you for your patronage.")"""

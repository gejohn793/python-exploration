#orint and input test
age_string = input("How old are you? ")
age = int(age_string)
print("Next year, you'll be", age + 1, "years old")

#passing multiple arguments in function
print(1, 2, 3, 4, 5, "and that's it!")

#incorrectly attempting to pass 2 arguments with input function
#input("How old are you? ", "Extra thing")

#len function and if statements
name = input("Please type your first name: ")
len_name = len(name)

if len(name) > 8:
    print("You have a long name!")
else:
    print("Your name is nice and short.")

print(type(len(name)))


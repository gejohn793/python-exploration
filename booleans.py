this_is_false = 3<2
print(this_is_false)

if "":
    print("Empty string is considered true")
else:
    print("Empty string is considered false")

if 0:
    print("0 is considered true")
else:
    print("0 is considered false")
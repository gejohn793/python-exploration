#first practice of setting variables
message = "This is a string in a Python file"
some_number = 1928
print("The message is \"" + message + "\"")
print("The number is", some_number)

#evaulating type and equality
number_three = 3
string_three = "3"
print(number_three)
print(string_three)
print(number_three == string_three) #will return false since numbers =/= strings

#adding different variable types, will always error, correction below
#print(number_three + (string_three)

#convert string into integer using int function
converted_three = int(string_three)
print(converted_three == number_three)

#getting input from terminal
response = input("How many walls are in your room? ")
print("You typed:", response)
print("The value is a" , type(response))

#converting string to int with input from terminal
num_walls = int(response)
print("My room has", num_walls + 1, "walls")

#concatenation
title = "Dr. "
name = "Syed"
combined = title + name
print (combined)